<!--
SPDX-FileCopyrightText: 2023 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Instructions

1. Create encoded diagram:

```
cat workshop_materials/assets/img/6-pipeline-performance/dag-stages.diag | python -c "import sys; import base64; import zlib; print(base64.urlsafe_b64encode(zlib.compress(sys.stdin.read().encode('utf-8'), 9)).decode('ascii'))"
```

> eNqVjkEOwjAMBO99hZUHICpOtOItKA1WsAh2lLgHVPXvpBHcCIKbtTve3SmIu13Ielg6AEmErFZJGE4QJWmypGNXLJ9kjhUCCHbCUAATiBWyWo9mrE4rYPMK7ZAznp3cYyDL7v1Vc-q9tpoU8x9NGz3Eh16Fh8PuaD7K_b6h99-npJl_X1LgV9raPQHrpWzo

2. Add hash to URL and use it to display the diagram:

```
![DAG with stages](https://kroki.hzdr.de/blockdiag/svg/eNqVjkEOwjAMBO99hZUHICpOtOItKA1WsAh2lLgHVPXvpBHcCIKbtTve3SmIu13Ielg6AEmErFZJGE4QJWmypGNXLJ9kjhUCCHbCUAATiBWyWo9mrE4rYPMK7ZAznp3cYyDL7v1Vc-q9tpoU8x9NGz3Eh16Fh8PuaD7K_b6h99-npJl_X1LgV9raPQHrpWzo)
```

![DAG with stages](https://kroki.hzdr.de/blockdiag/svg/eNqVjkEOwjAMBO99hZUHICpOtOItKA1WsAh2lLgHVPXvpBHcCIKbtTve3SmIu13Ielg6AEmErFZJGE4QJWmypGNXLJ9kjhUCCHbCUAATiBWyWo9mrE4rYPMK7ZAznp3cYyDL7v1Vc-q9tpoU8x9NGz3Eh16Fh8PuaD7K_b6h99-npJl_X1LgV9raPQHrpWzo)

Source: https://docs.kroki.io/kroki/setup/usage/
