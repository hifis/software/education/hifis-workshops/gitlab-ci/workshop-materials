<!--
SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction to Continuous Integration

## The Story

A team of scientists is working on a little project that takes
astronaut data from Wikidata to analyse the time humans spent in space
as well as the age distribution of the astronauts.
The project quickly gained attraction and a lot of users as well as contributors
joined the project.
After some time it became hard for the maintainers to ensure new functionality is
properly tested.
It also frequently happened that contributors followed a different code style or
forgot to add license information.

Verifying those criteria manually is tedious and not promising in the long run.
This is why the team aims at automating as much as possible
to save their valuable time.
Luckily, they found a tool called *GitLab CI* which they can use to automate
those tasks.
In the following we will learn what GitLab CI and Continuous Integration
is all about.

<figure>
  <img src="../../assets/img/01-introduction/is_it_worth_the_time.png" alt="XKCD - Is it worth the time?"/>
  <figcaption>
    <a href="https://xkcd.com/1205/">Is it worth the time? -</a>
    licensed under a
    <a href="https://creativecommons.org/licenses/by-nc/2.5">
        Creative Commons Attribution-NonCommercial 2.5 License
    </a>.
  </figcaption>
</figure>

This comic shows the relation between duration of a task, its repetitions and
the time this task consumes - if task-execution is done by hand.
We are speaking of weeks or even months of work over the course of time.
Automating this part of the process thus can save a substantial amount of time.

## Definition

> Continuous Integration is a software development practice
> where members of a team integrate their work frequently,
> usually each person integrates at least daily -
> leading to multiple integrations per day.  
> &mdash; <cite>[Martin Fowler][1]</cite>

## Practices of Continuous Integration

1. Maintain a Single Source Repository

    * Software projects involve lots of files
      that need to be orchestrated together to build a product.
      Keeping track of all of these is a major effort,
      particularly when multiple people are involved.
    * Make sure to maintain a single source code management system like Git.

2. Automate the Build

    * Getting the sources turned into a running system can
      often be a complicated process involving compilation,
      moving files around, loading schemas into the databases, and so on.
    * Automated environments for builds are a common feature of systems.
    * Make sure you can build and launch your system
      using these scripts using a single command.

3. Make Your Build Self-Testing

    * Traditionally, a build means compiling, linking, and all the additional stuff
      required to get a program to execute.
      A program may run, but that doesn't mean it does the right thing.
    * A good way to catch bugs more quickly and efficiently
      is to include automated tests in the build process.

4. Everyone Commits To the Mainline Every Day

    * Integration is primarily about communication.
      Integration allows developers to tell other developers about the changes they have made.
      Frequent communication allows people to know quickly as changes develop.
    * By doing this frequently, developers quickly find out if
      there's a conflict between two developers.

5. Every Commit Should Build the Mainline on an Integration Machine

    * Using daily commits, a team gets frequent tested builds.
      This ought to mean that the mainline stays in a healthy state.
      In practice, however, things still do go wrong.
    * As a result you should ensure that regular builds happen on an integration machine.

6. Fix Broken Builds Immediately

    * A key part of doing a continuous build is that if the mainline build fails,
      it needs to be fixed right away.
    * A phrase Kent Beck used to say was "nobody has a higher priority task
      than fixing the build".

7. Keep the Build Fast

    * The whole point of Continuous Integration is to provide rapid feedback.
    * For most projects, however, the XP guideline of a ten minutes build
      is perfectly within reason.

8. Test in a Clone of the Production Environment

    * The point of testing is to flush out, under controlled conditions,
      any problem that the system will have in production.
    * If you test in a different environment,
      every difference results in a risk that what happens
      under test won't happen in production.

9. Make it Easy for Anyone to Get the Latest Executable

    * People find it much easier to see something
      that's not quite right and say how it needs to be changed.
    * To help make this work, anyone involved with a software project
      should be able to get the latest executable and be able to run it:
      for demonstrations, exploratory testing, or just to see what changed this week.

10. Everyone can see what's happening

    * Continuous Integration is all about communication,
        so you want to ensure that everyone can easily see the state of the system
        and the changes that have been made to it.
    * One of the most important things to communicate is the state of the mainline build.

11. Automate Deployment

    * It's important to have scripts that will allow you
      to deploy the application into any environment easily.
    * A natural consequence of this is that you should also have scripts
      that allow you to deploy into production with similar ease.

Practices taken from [Martin Fowler][1].

## What does CI/CD refer to?

Taken from [GitLab][2].

### Continuous Integration (CI)

Continuous integration is the practice of integrating all your code changes
into the main branch of a shared source code repository early and often,
automatically testing each change when you commit or merge them,
and automatically kicking off a build.
With continuous integration,
errors and security issues can be identified and fixed more easily,
and much earlier in the software development lifecycle.

### Continuous Delivery (CD)

Continuous delivery is a software development practice
that works in conjunction with continuous integration
to automate the infrastructure provisioning and application release process.

Once code has been tested and built as part of the CI process,
continuous delivery takes over during the final stages to ensure
it is packaged with everything it needs
to deploy to any environment at any time.
Continuous delivery can cover everything from provisioning the infrastructure
to deploying the application to the testing or production environment.

With continuous delivery, the software is built
so that it can be deployed to production at any time.
Then, you can trigger the deployments manually or
move to continuous deployment where deployments are automated as well.

### Continuous Deployment

Continuous deployment (CD) is a software engineering approach
in which software functionalities are delivered frequently through automated deployments.

## Popular Tools

* [GitLab CI](https://docs.gitlab.com/ee/ci/)
* [GitHub Actions](https://docs.github.com/en/actions)
* [Jenkins](https://www.jenkins.io/doc/)
* [Circle CI](https://circleci.com/docs/)
* [Concourse CI](https://concourse-ci.org/)

[1]: https://martinfowler.com/articles/continuousIntegration.html
[2]: https://about.gitlab.com/topics/ci-cd/
[cc-by-nc-2.5]: https://creativecommons.org/licenses/by-nc/2.5/
