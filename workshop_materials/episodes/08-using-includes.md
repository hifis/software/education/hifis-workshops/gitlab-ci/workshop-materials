<!--
SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# More Concise Pipeline Definition and Cross-Project Reuse

## Gems and Jewels to Collect

In this episode we will discuss ways to take a CI pipeline apart and
extract parts of the pipeline into separate _YAML_ files that can be included
into the main `.gitlab-ci.yml` _YAML_ file or even reused in other projects in
the same or different _GitLab_ instances.
Finally, we will show you how to benefit from existing GitLab CI/CD Templates
and include these in your pipeline.

## Introduction

With time your CI pipeline might become large with several hundred
_lines of code_ and the pipeline might lack some structuring.
You may split it up into multiple _YAML_ files to give it a structure 
that is easier to handle and to arrange it more clearly.
These _YAML_ files can also be reused in different places, i.e.
projects in the same or even in different _GitLab_ instances.

## `include` _YAML_ files

The
[`include` keyword](https://docs.gitlab.com/ee/ci/yaml/#include)
used here is the keyword of your choice if you want to include _YAML_ files
into another _YAML_ file.
Imagine you have defined a _YAML_ file that appears to be useful for many
_Python_ projects: An example could be a _YAML_ file `common.gitlab-ci.yml`
that contains all common aspects of the CI pipeline definition relevant
for all CI jobs and is put into directory `.gitlab/ci/` of your _Python_
project:

```yaml
stages:
  - lint
  - test
  - run
  - deploy

default:
  interruptible: true

variables:
  PY_COLORS: "1"
  CACHE_PATH: "$CI_PROJECT_DIR/.cache"
  UV_CACHE_DIR: "$CACHE_PATH/uv"
  UV_PROJECT_ENVIRONMENT: "$CACHE_PATH/venv"
  PIP_CACHE_DIR: "$CACHE_PATH/pip"

.base_image:
  image: python:3.12

.base_rules:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.dependencies:
  before_script:
    - pip install --upgrade pip
    - pip install uv
    - uv venv $UV_PROJECT_ENVIRONMENT
    - source $UV_PROJECT_ENVIRONMENT/bin/activate
    - uv sync --group dev --active --frozen
  cache:
    key:
      files:
        - uv.lock
      prefix: $CI_JOB_IMAGE
    paths:
      - "$UV_CACHE_DIR"
      - "$UV_PROJECT_ENVIRONMENT"
      - "$PIP_CACHE_DIR"
```

It can be included into the main `.gitlab-ci.yml` _YAML_ file as follows:

```yaml
include:
  - local: '.gitlab/ci/common.gitlab-ci.yml'
```

As you can see the keyword `include` has got sub-keys namely `local`,
`file`, `remote` and `template` which we will explore in the next sections.

### Include Files Inside the Same Project with `include:local`

As demonstrated above, the simplest way to include _YAML_ files in another one
is to put the file to include somewhere in the same project and reference it
with the 
[`include:local` sub-key](https://docs.gitlab.com/ee/ci/yaml/#includelocal)
in the main _YAML_ file `.gitlab-ci.yml` by using the relative
path from the project root to the _YAML_ file to include.

### Include Files from the Same GitLab Instance with `include:file`

The extracted _YAML_ file might not be contained in the same project but in
a different project in the same GitLab instance.
The corresponding reusable _YAML_ given above can be referenced with the 
[`include:file` sub-key](https://docs.gitlab.com/ee/ci/yaml/#includefile)
like this:

```yaml
include:
  - project: 'my-group/my-project'
    ref: 'main'
    file: '.gitlab/ci/common.gitlab-ci.yml'
```

### Include Files from Other GitLab Instances with `include:remote`

If the reusable _YAML_ file is not even located in the same GitLab instance,
we could make use of the _YAML_ with the
[`include:remote` sub-key](https://docs.gitlab.com/ee/ci/yaml/#includeremote):

```yaml
include:
  - remote: 'https://gitlab.com/my-group/my-project/-/raw/main/.gitlab/ci/common.gitlab-ci.yml'
```

### Include GitLab CI/CD Templates with `include:template`

If your use-case is a common one, you could also use one of the
[GitLab CI/CD Templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates)
of the _GitLab_ project on _GitLab.com_ with the
[`include:template` sub-key](https://docs.gitlab.com/ee/ci/yaml/#includetemplate):

```yaml
include:
  - template: 'Python.gitlab-ci.yml'
```

At the time being the above-mentioned
[Python.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml)
template looks like this:

```yaml
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# https://pip.pypa.io/en/stable/topics/caching/
cache:
  paths:
    - .cache/pip

before_script:
  - python --version ; pip --version  # For debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate

test:
  script:
    - pip install ruff tox  # you can also use tox
    - pip install --editable ".[test]"
    - tox -e py,ruff

run:
  script:
    - pip install .
    # run the command here
  artifacts:
    paths:
      - build/*

pages:
  script:
    - pip install sphinx sphinx-rtd-theme
    - cd doc
    - make html
    - mv build/html/ ../public/
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:
  stage: deploy
  script: echo "Define your deployment script!"
  environment: production
```

## Split the Pipeline Definitions into Multiple Files

Now we are going to split our rather long `.gitlab-ci.yml` _YAML_ file into
multiple _YAML_ files to:

* increase readability and maintainability, 
* reduce duplication of the same configuration in multiple places,
* reuse complete _YAML_ files in different projects of the same kind.

After having already separated the common pipeline definitions, let us split
the rest of the pipeline into four files, one for each stage:

1. File `.gitlab/ci/lint.gitlab-ci.yml` defines all CI jobs in stage `lint`:
    ```yaml
    license-compliance:
      stage: lint
      extends:
        - .base_image
        - .dependencies
      script:
        - uv run reuse lint
      needs: []

    codestyle:
      stage: lint
      extends:
        - .base_image
        - .dependencies
      script:
        - uv run black --check --diff .
        - uv run isort --check --diff .
      needs: []
    ```
2. File `.gitlab/ci/test.gitlab-ci.yml` defines all CI jobs in stage `test`:
    ```yaml
    test-python:
      image: python:${PYTHON_VERSION}
      stage: test
      extends:
        - .dependencies
      script:
        - uv run pytest tests/
      parallel:
        matrix:
          - PYTHON_VERSION: ["3.11", "3.12", "3.13"]
      needs: []
    ```
3. File `.gitlab/ci/run.gitlab-ci.yml` defines all CI jobs in stage `run`:
    ```yaml
    analysis:
      extends:
        - .base_image
        - .dependencies
        - .base_rules
      stage: run
      script:
        - uv run python -m astronaut_analysis
      artifacts:
        paths:
          - results/
      needs: ["test-python: [3.12]"]
      interruptible: false
    ```
4. File `.gitlab/ci/deploy.gitlab-ci.yml` defines all CI jobs in stage `deploy`:
    ```yaml
    pages:
      extends:
        - .base_rules
      stage: deploy
      script:
        - mkdir public/
        - cp results/* public/
      artifacts:
        paths:
          - public/
      dependencies:
        - analysis
      interruptible: false
    ```

Now, to include all those five _YAML_ files in file `.gitlab-ci.yml`, you can list
all files to include with the `include:local` sub-key:

```yaml
include:
  - local: '.gitlab/ci/common.gitlab-ci.yml'
  - local: '.gitlab/ci/lint.gitlab-ci.yml'
  - local: '.gitlab/ci/test.gitlab-ci.yml'
  - local: '.gitlab/ci/run.gitlab-ci.yml'
  - local: '.gitlab/ci/deploy.gitlab-ci.yml'
```

This way your main CI-file stays clearly arranged, and it is easier to orient
and get a good first overview.
If you need to look deeper into the structure you will very easily spot
the file in which you need to look into.

## Make Use of GitLab CI/CD Templates

The
[GitLab CI/CD Templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates)
of the _GitLab_ project on _GitLab.com_ offer a huge library of sophisticated
reference templates.
You can use an original or a modified version of a template and create a
_YAML_ file to be included, e.g. with the `include:local` sub-key.
You can also include the file as is into your CI pipeline with the
`include:template` sub-key if it already works out of the box for your project.
Be aware that these templates might be subject to change over the course of
time if you consider to include templates from _GitLab_.

??? question "Exercise"

    ## Exercise 1 - Restructure the CI Pipeline of the Exercise Project
    
    In this final episode we used the `include` keyword to split up a CI pipeline
    into multiple files.
    Now try to apply this way of restructuring on the current version of our
    exercise CI pipeline.

!!! success "Take Home Messages"

    Our last episode about mastering GitLab CI pipelines was about how to
    structure the `.gitlab-ci.yml` file even more by splitting it up into
    several files that can be included in the main `.gitlab-ci.yml` file.

## How to Continue?

Your path to become a master in GitLab CI pipelines has been laid out.
These resources will help you when you are about to develop CI pipelines
in your own projects.
In the next session of this workshop we will focus on _your_ code projects and
help you to get started with your CI projects.
After that you will be able to dive deeper into the topic on your own by
working on your specific requirements of your CI pipelines and reading
more in the official GitLab documentation about
[concepts](https://docs.gitlab.com/ee/ci/),
[keywords](https://docs.gitlab.com/ee/ci/yaml/), and
[templates](https://docs.gitlab.com/ee/ci/examples/).
