<!--
SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Getting Started with GitLab CI

## Prerequisites

* A project in GitLab that you would like to use CI/CD for.
* The Maintainer or Owner role for the project.
* Make sure you have [GitLab CI runners](https://docs.gitlab.com/runner/)
  available that can execute your jobs.
  This is already the case in [Helmholtz Codebase](https://codebase.helmholtz.cloud).

## Let's Create Our First `.gitlab-ci.yml` File

It all starts with creating a `.gitlab-ci.yml` file in the root of your repository.
In this file you define:

* The structure and order of jobs that the runner executes.
* The decisions a runner should take when certain conditions are met.

For example, you might want to run certain tasks only if they are executed on the
main branch.
In addition to testing the application you might also want to publish it
on changes to the main branch.

To create our first `.gitlab-ci.yml` follow these steps:

1. [Create a new GitLab project.](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project)  
   ![Create new project screenshot.](../../assets/img/2-getting-started/create_project.png)
2. On the left sidebar, click **CI/CD > Editor**.
3. Click the _Configure pipeline_ button.
4. The pipeline editor opens with a test pipeline prefilled.
5. Use the **Commit changes** button at the bottom of the page to run the pipeline.

For reference, the content of the test `.gitlab-ci.yml` file is given below.

    ```yaml
    # This file is a template, and might need editing before it works on your project.
    # This is a sample GitLab CI/CD configuration file that should run without any modifications.
    # It demonstrates a basic 3 stage CI/CD pipeline. Instead of real tests or scripts,
    # it uses echo commands to simulate the pipeline execution.
    #
    # A pipeline is composed of independent jobs that run scripts, grouped into stages.
    # Stages run in sequential order, but jobs within stages run in parallel.
    #
    # For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages
    #
    # You can copy and paste this template into a new `.gitlab-ci.yml` file.
    # You should not add this template to an existing `.gitlab-ci.yml` file by using the `include:` keyword.
    #
    # To contribute improvements to CI/CD templates, please follow the Development guide at:
    # https://docs.gitlab.com/ee/development/cicd/templates.html
    # This specific template is located at:
    # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Getting-Started.gitlab-ci.yml
    
    stages:          # List of stages for jobs, and their order of execution
      - build
      - test
      - deploy
    
    build-job:       # This job runs in the build stage, which runs first.
      stage: build
      script:
        - echo "Compiling the code..."
        - echo "Compile complete."
    
    unit-test-job:   # This job runs in the test stage.
      stage: test    # It only starts when the job in the build stage completes successfully.
      script:
        - echo "Running unit tests... This will take about 60 seconds."
        - sleep 60
        - echo "Code coverage is 90%"
    
    lint-test-job:   # This job also runs in the test stage.
      stage: test    # It can run at the same time as unit-test-job (in parallel).
      script:
        - echo "Linting code... This will take about 10 seconds."
        - sleep 10
        - echo "No lint issues found."
    
    deploy-job:      # This job runs in the deploy stage.
      stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
      environment: production
      script:
        - echo "Deploying application..."
        - echo "Application successfully deployed."
    ```

The CI Pipeline editor looks like this:

![CI Pipeline Editor](../../assets/img/2-getting-started/pipeline-editor.png)

## View the Status of Our First Pipeline

* To view the status of the CI pipeline, on the left sidebar
  click **CI/CD > Pipelines**.
  A pipeline with three different stages shows up.  
  ![Pipeline Overview UI.](../../assets/img/2-getting-started/pipelines-overview.png)
* By clicking on the pipeline ID you will get a visual representation of the pipeline.  
  ![Visual representation of the CI pipeline.](../../assets/img/2-getting-started/pipeline-representation.png)
* View the details of a job by clicking on the job name,
  e.g. `unit-test-job`.  
  ![Job details in the GitLab UI.](../../assets/img/2-getting-started/job-details.png)
* Pipelines can be retried by clicking on the _Retry_ button.

## `.gitlab-ci.yml` Taken Apart

Let's break the example into parts to understand some concepts.

* The `.gitlab-ci.yml` file defines the *Pipeline*.
* A *Pipeline* is the top-level component of CI.

The pipeline comprises

* Jobs, which define *what* to do (e.g. `test-job2`).
  For example, jobs that compile code or deploy the application.
* Stages, which define *when* to run the jobs.
  For example, stages that run tests after having compiled the code
  (e.g. `build` or `test`).

We will introduce the keywords (e.g. `script`) used in the sample
`.gitlab-ci.yml` file in the next episode of the workshop.

## GitLab CI/CD Workflow

<figure>
  <img src="../../assets/img/2-getting-started/gitlab_workflow_example_11_9.png" alt="GitLab CI/CD Workflow"/>
  <figcaption>
    GitLab CI/CD Workflow - licensed under a
    <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">
        Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license © GitLab
    </a>.
  </figcaption>
</figure>

In the example workflow above GitLab exemplifies a common use case of a
contributor who triggers a CI pipeline by pushing changes to a feature
branch.
In this example the pipeline fails so that the contributor needs to fix
part of the code and rerun the pipeline until it succeeds.
As soon as the feature branch is merged into the main branch, the CI
pipeline runs again and tests that the CI pipeline also succeeds on the
main branch into which these changes have been integrated.

!!! success "Take Home Messages"

    * The `.gitlab-ci.yml` file defines the *Pipeline*.
    * Adding a `.gitlab-ci.yml` to the root of your repository
      enables GitLab CI/CD for your project.
    * A *Pipeline* is the top-level component of GitLab CI.
    * A *Pipeline* consists of
        * Jobs, which define *what* to do.
        * Stages, which define *when* to run the jobs.
