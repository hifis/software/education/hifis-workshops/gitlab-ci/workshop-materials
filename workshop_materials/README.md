<!--
SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Continuous Integration in GitLab

## Summary

* **Language:** English
* **Level:** Intermediate
* **Prerequisites:**
    * Git
    * GitLab
    * Unix Shell
    * Basic programming skills

## Workshop Content

A team of scientists is working on a little project
that takes astronaut data from Wikidata to analyse the time humans spent in space
as well as the age distribution of the astronauts.
The project quickly gained attraction and a lot of users as well as contributors joined the project.
After some time it became hard for the maintainers to ensure new functionality is properly tested.
It also frequently happened that contributors followed a different code style
or forgot to add license information.

Verifying those criteria manually is tedious and not promising in the long run.
This is why the team aims at automating as much as possible to save their valuable time.
Luckily, they found a tool called GitLab CI which they can use to automate those tasks.
In the following we will learn what GitLab CI and Continuous Integration is all about.

Day 1 will focus on creating an initial GitLab CI pipeline.
Building on top of this the course will use day 2 to learn advanced concepts of GitLab CI useful for optimizing the pipeline.
Day 3 of the workshop is reserved for working on your own projects together with experienced mentors.

More detailed information about the course structure is available in the timetable.

### Requirements

Participants should be familiar with the basic operations of [Git](https://swcarpentry.github.io/git-novice/)
and [GitLab](https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/software-development-for-teams/workshop-materials)
and ideally already have made some initial experience with a [Unix-shell](https://swcarpentry.github.io/shell-novice/).

No further software needs to be installed.
Please make sure that you can log in to <codebase.helmholtz.cloud> via the Helmholtz AAI
before participating in the workshop.
